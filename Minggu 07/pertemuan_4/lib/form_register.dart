import 'package:flutter/material.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Register'),
        backgroundColor: Colors.lightBlue,
      ),
      body: ListView(
        key: _formKey,
        children: [
          Text('Register Your Account'),
          Padding(padding: new EdgeInsets.only(top: 20.0)),
          Container(
            child: Column(
              children: <Widget>[
                TextFormField(
                    autofocus: true,
                    decoration: InputDecoration(
                        hintText: "Name",
                        labelText: "Name",
                        icon: Icon(Icons.person),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Nama Cant Be Empty';
                      }
                      return null;
                    }),
                Padding(padding: EdgeInsets.only(top: 10.0)),
                TextFormField(
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                    hintText: "Email",
                    labelText: "Email",
                    icon: Icon(Icons.password),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0)),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
