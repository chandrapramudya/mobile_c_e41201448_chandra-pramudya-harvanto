void main() {
  var hari = 27;
  var bulan = 7;
  var tahun = 2000;

  switch (bulan) {
    case 1:
      print("$hari Januari $tahun");
      break;
    case 2:
      print("$hari Februari $tahun");
      break;
    case 3:
      print("$hari Maret $tahun");
      break;
    case 4:
      print("$hari April $tahun");
      break;
    case 5:
      print("$hari Mei $tahun");
      break;
    case 6:
      print("$hari Juni $tahun");
      break;
    case 7:
      print("$hari Juli $tahun");
      break;
    default:
      print("Bulan Tidak Tersedia");
  }
}
