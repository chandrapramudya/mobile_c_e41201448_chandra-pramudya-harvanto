import 'dart:io';

void main(List<String> args) {
  print("Masukan Nama Anda: ");
  var nama = stdin.readLineSync()!;
  print("Masukkan Peran Anda: ");
  var peran = stdin.readLineSync()!;

  if (nama == "") {
    print("Nama harus diisi!");
  } else if (peran == "") {
    print("Peran harus diisi!");
  } else {
    print("Nama = " + nama);
    print("Peran = " + peran);
  }
}
