import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    title: 'Testing Color Gradient',
    home: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Flexible(
              flex: 1,
              child: Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.bottomLeft,
                    end: Alignment.topRight,
                    colors: <Color>[
                      Colors.teal,
                      Colors.lightBlue,
                    ],
                  ),
                ),
              )),
          Flexible(
              flex: 2,
              child: Container(
                decoration: BoxDecoration(
                  gradient: RadialGradient(
                    radius: 1.0,
                    colors: <Color>[
                      Colors.yellow,
                      Colors.deepPurple,
                      Colors.blue
                    ],
                  ),
                ),
              )),
          Flexible(
              flex: 1,
              child: Container(
                decoration: BoxDecoration(
                  gradient: SweepGradient(startAngle: 1.0, colors: <Color>[
                    Colors.lime,
                    Colors.cyan,
                    Colors.amber,
                  ]),
                ),
              )),
        ],
      ),
    );
  }
}
