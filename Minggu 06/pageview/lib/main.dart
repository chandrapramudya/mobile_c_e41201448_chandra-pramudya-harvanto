import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

void main() {
  runApp(MaterialApp(
    home: new Home(),
    debugShowCheckedModeBanner: false,
  ));
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final List<String> gambar = ["random.gif", "trashcan.gif", "XaDe.gif"];

  static const Map<String, Color> colors = {
    'random': Color(0xFF2DB569),
    'trashcan': Color(0xFFF386B8),
    'XaDe': Color(0xFF45CAF5),
  };

  @override
  Widget build(BuildContext context) {
    timeDilation = 5.0;
    return new Scaffold(
      body: new Container(
        decoration: new BoxDecoration(
          gradient: new LinearGradient(
              begin: FractionalOffset.topCenter,
              end: FractionalOffset.bottomCenter,
              colors: [Colors.white, Colors.lightBlue, Colors.blueGrey]),
        ),
        child: new PageView.builder(
            controller: new PageController(viewportFraction: 0.8),
            itemCount: gambar.length,
            itemBuilder: (BuildContext context, int i) {
              return new Padding(
                  padding:
                      new EdgeInsets.symmetric(horizontal: 5.0, vertical: 50.0),
                  child: new Material(
                    elevation: 8.0,
                    child: new Stack(
                      fit: StackFit.expand,
                      children: <Widget>[
                        new Hero(
                            tag: gambar[i],
                            child: new Material(
                              child: new InkWell(
                                child: new Flexible(
                                  flex: 1,
                                  child: Container(
                                    color: colors.values.elementAt(i),
                                    child: new Image.asset(
                                      "img/${gambar[i]}",
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                                onTap: () => Navigator.of(context).push(
                                    new MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            new HalamanDua(
                                              gambar: gambar[i],
                                              colors:
                                                  colors.values.elementAt(i),
                                            ))),
                              ),
                            ))
                      ],
                    ),
                  ));
            }),
      ),
    );
  }
}

class HalamanDua extends StatelessWidget {
  HalamanDua({required this.gambar, required this.colors});
  final String gambar;
  final Color colors;
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Data 01"),
        backgroundColor: Colors.purpleAccent,
      ),
      body: new Stack(
        children: <Widget>[
          new Container(
            decoration: new BoxDecoration(
                gradient: new RadialGradient(
                    center: Alignment.center,
                    colors: [Colors.purple, Colors.white, Colors.deepPurple])),
          ),
          new Center(
            child: Hero(
                tag: gambar,
                child: ClipOval(
                    child: SizedBox(
                        width: 200.0,
                        height: 200.0,
                        child: Material(
                          child: InkWell(
                            onTap: () => Navigator.of(context).pop(),
                            child: Flexible(
                              flex: 1,
                              child: Container(
                                color: colors,
                                child: Image.asset(
                                  "img/$gambar",
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ),
                        )))),
          )
        ],
      ),
    );
  }
}
