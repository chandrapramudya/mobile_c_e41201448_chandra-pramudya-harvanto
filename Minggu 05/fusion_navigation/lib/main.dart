import 'package:flutter/material.dart';
import 'package:fusion_navigation/bottom.dart';
import 'package:fusion_navigation/home.dart';
import 'package:fusion_navigation/profil.dart';
import 'package:fusion_navigation/search.dart';
import 'package:fusion_navigation/setting.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: BottomNav(),
      routes: {
        "/home": (BuildContext context) => const home(),
        "/profil": (BuildContext context) => const profil(),
        "/setting": (BuildContext context) => const setting(),
      },
    );
  }
}
