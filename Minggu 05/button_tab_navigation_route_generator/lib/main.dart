import 'package:flutter/material.dart';
import 'package:button_tab_navigation/Route.dart';

void main() {
  runApp(MaterialApp(
    onGenerateRoute: RouteGenerator.generateRoute,
  ));
}
