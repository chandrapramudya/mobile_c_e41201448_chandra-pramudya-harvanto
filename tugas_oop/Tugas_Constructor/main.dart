import 'employee.dart';

void main(List<String> args) {
  var NIM = new employee.nim("E41201448");
  var Nama = new employee.nama("Chandra Pramudya Harvanto");
  var jurusan = new employee.departemen("Teknik Informatika");

  print("NIM = ${NIM.id}");
  print("Nama = ${Nama.name}");
  print("Prodi = ${jurusan.department}");
}
