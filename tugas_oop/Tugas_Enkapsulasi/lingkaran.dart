class Bangun_lingkaran {
  double jari_jari = 0.0;
  double phi = 3.14;

  void setjari_jari(double value) {
    if (value < 0) {
      value *= -1;
    }
    jari_jari = value;
  }

  double getjare_jari() {
    return jari_jari;
  }

  double rumusluas() {
    return this.phi * jari_jari * jari_jari;
  }
}
