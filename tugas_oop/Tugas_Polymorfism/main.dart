import 'persegi.dart';
import 'segitiga.dart';
import 'lingkaran.dart';

void main(List<String> args) {
  persegi p = new persegi(5);
  segitiga s = new segitiga(3, 4, 5);
  lingkaran l = new lingkaran(30);

  print("Luas Persegi = ${p.luas()}");
  print("Keliling Persegi = ${p.keliling()}");
  print("Luas Segitiga = ${s.luas()}");
  print("Keliling Segitiga = ${s.keliling()}");
  print("Luas Lingkaran = ${l.luas()}");
  print("Keliling Lingkaran = ${l.keliling()}");
}
