import 'BangunDatar.dart';

class segitiga extends BangunDatar {
  double alas = 0;
  double tinggi = 0;
  double s_miring = 0;

  segitiga(double alas, double tinggi, double s_miring) {
    this.alas = alas;
    this.tinggi = tinggi;
    this.s_miring = s_miring;
  }

  @override
  double luas() {
    return 0.5 * alas * tinggi;
  }

  @override
  double keliling() {
    return alas + s_miring + tinggi;
  }
}
