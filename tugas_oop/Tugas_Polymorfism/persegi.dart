import 'BangunDatar.dart';

class persegi extends BangunDatar {
  double sisi1 = 0;

  persegi(double sisi1) {
    this.sisi1 = sisi1;
  }

  @override
  double luas() {
    return sisi1 * sisi1;
  }

  @override
  double keliling() {
    return sisi1 * 4;
  }
}
