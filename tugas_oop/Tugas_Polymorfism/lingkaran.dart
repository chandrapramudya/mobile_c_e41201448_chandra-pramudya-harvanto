import 'BangunDatar.dart';

class lingkaran extends BangunDatar {
  double r = 0;

  lingkaran(double r) {
    this.r = r;
  }

  @override
  double luas() {
    return 3.14 * r * r;
  }

  @override
  double keliling() {
    return 2 * 3.14 * r;
  }
}
