import 'armor_titan.dart';
import 'attack_titan.dart';
import 'beast_titan.dart';
import 'human.dart';

void main(List<String> args) {
  Armor_Titan AT = new Armor_Titan();
  Attack_Titan ATKT = new Attack_Titan();
  Beast_Titan BT = new Beast_Titan();
  Human H = new Human();

  AT.powerPoint = 3;
  ATKT.powerPoint = 5;
  BT.powerPoint = 4;
  H.powerPoint = 5;

  print("Power Armor Titan = ${AT.powerPoint}");
  print("Armor Titan = ${AT.terjang()}");
  print("Power Attack Titan = ${ATKT.powerPoint}");
  print("Attack Titan = ${ATKT.punch()}");
  print("Power Beast Titan = ${BT.powerPoint}");
  print("Beast Titan = ${BT.lempar()}");
  print("Power Human = ${H.powerPoint}");
  print("Human = ${H.killAllTitan()}");
}
